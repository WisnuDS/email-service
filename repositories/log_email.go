package repositories

import (
	"context"
	"email.example.com/v1/configs"
	"email.example.com/v1/models"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type LogEmailRepository struct {
	db *mongo.Database
}

func NewLogEmailRepository() *LogEmailRepository {
	return &LogEmailRepository{
		db: configs.GetConnection(),
	}
}

func (l *LogEmailRepository) Create(email string, message string) error{
	model := models.LogEmail{
		Email: email,
		Message: message,
		SendAt: time.Now(),
	}
	_, err := l.db.Collection("log_email").InsertOne(context.TODO(), model)
	if err != nil {
		return err
	}

	return nil
}