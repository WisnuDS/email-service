package services

import (
	"email.example.com/v1/repositories"
	"fmt"
	"time"
)

func SendEmail(email string, message string)  {
	time.Sleep(10 * time.Second)
	repo := repositories.NewLogEmailRepository()
	err := repo.Create(email, message)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("Message send to email %s\n", email)
}
