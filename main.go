package main

import (
	"email.example.com/v1/configs"
	"email.example.com/v1/services"
	"encoding/json"
	"fmt"
)

func main() {
	channel := configs.GetChannel()
	queue, err := channel.QueueDeclare(
		"testing",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		panic(err)
	}

	message, err := channel.Consume(
		queue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return
	}

	forever := make(chan bool)

	go func() {
		for d := range message {
			fmt.Println("New message from publisher")
			message := struct {
				Email string `json:"email"`
				Message string `json:"message"`
			}{}
			err := json.Unmarshal(d.Body, &message)
			if err != nil {
				fmt.Println("Error when parse message")
			}

			services.SendEmail(message.Email, message.Message)
		}
	}()
	<-forever
}
