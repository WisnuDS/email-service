package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type LogEmail struct {
	Id primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Email string `bson:"email" json:"email"`
	Message string `bson:"message" json:"message"`
	SendAt time.Time `bson:"send_at" json:"send_at"`
}
