module email.example.com/v1

go 1.16

require (
	github.com/streadway/amqp v1.0.0
	go.mongodb.org/mongo-driver v1.8.4
)
