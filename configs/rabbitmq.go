package configs

import (
	"fmt"
	"github.com/streadway/amqp"
	"os"
)

var channel *amqp.Channel

func init() {
	if channel == nil {
		URI := os.Getenv("RABBITMQ_URI")
		if URI == "" {
			URI = "amqp://guest:guest@localhost:5672"
		}

		fmt.Println("Connecting to Rabbitmq")

		conn, err := amqp.Dial(URI)
		if err != nil {
			panic(err)
		}

		channel, err = conn.Channel()
		if err != nil {
			panic(err)
		}
	}
}

func GetChannel() *amqp.Channel {
	return channel
}