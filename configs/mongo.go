package configs

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
)

var client *mongo.Client
func init() {
	if client == nil {
		println("Connect to mongo...")
		URI := os.Getenv("MONGODB_URI")
		if URI == "" {
			URI = "mongodb://localhost:27017/"
		}

		client, _ = mongo.Connect(context.TODO(), options.Client().ApplyURI(URI))
		println("Mongo connected...")
	}
}

func GetConnection() *mongo.Database {
	DB := os.Getenv("DB_NAME")
	if DB == "" {
		DB = "blog"
	}

	return client.Database(DB)
}

func CloseConnection() error {
	if client == nil {
		return nil
	}

	if err := client.Disconnect(context.TODO());  err != nil {
		return err
	}

	fmt.Println("MongoDB Disconnected...")
	return nil
}
